version=5.4.13
language=Fortran 77
compiler_env=VT_F77
compiler_flags_env=VT_FFLAGS
compiler=
compiler_flags=
linker_flags=
libs= -lotf  -lz     
includedir=${includedir}
libdir=${libdir}
opari_bin=/Users/ahume/SynthSys/sbsi-dev/install/bin/opari
opari_tab_compiler=gcc
opari_tab_compiler_flags=-I/Users/ahume/SynthSys/sbsi-dev/install/include
pmpilib=
fmpilib=-lvt.fmpi
dynattlib=
compiler_iflags_gnu=-g -finstrument-functions
compiler_iflags_intel=-g -finstrument-functions
compiler_iflags_pathscale=-g -finstrument-functions
compiler_iflags_pgi=-Mprof=func
compiler_iflags_sun=-O -Qoption f90comp -phat
compiler_iflags_xl=-qdebug=function_trace
compiler_iflags_ftrace=-ftrace
inst_avail=manual pomp gnu
inst_default=gnu
