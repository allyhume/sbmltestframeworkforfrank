#!/bin/sh 
# Runs simulations
# Usage : runRHS.sh dataSeriesOutfileName   executable name

# E.g., runRHS.sh outfile  myModel_RHS.exe
mydir=$PWD
t_series_outfile=$1

model_name=UserModel

exec_file=$2


t_final=600.0
t_init=0.0
maxTimes=100000000000
interval=0.01
out_interval=0.1
atol=1.0e-14
reltol=1.0e-4


echo $exec_file \
	$model_name \
	$t_final \
	$t_init \
	$maxTimes \
	$interval \
	$out_interval \
	$atol \
	$reltol \
        $t_series_outfile 

##
#execute
##
mpirun $exec_file \
	$model_name \
	$t_final \
	$t_init \
	$maxTimes \
	$interval \
	$out_interval \
	$atol \
	$reltol \
        $t_series_outfile