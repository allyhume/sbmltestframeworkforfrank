/*****************************************************************************
 *
 *  SobolPolynomials.h
 *
 *  This is a table of binary-encoded primitive polynomials
 *  originating from Bratley and Fox, ACM TOMS, 14, 88-100 (1988).
 *
 *  See http://www.netlib.org/toms/659/
 *
 *  $Id: SobolPolynomials.h,v 1.1.2.1 2010/05/20 14:36:27 ayamaguc Exp $
 *
 *  Millar Laboratory, Centre for Systems Biology at Edinburgh
 *  (c) The University of Edinburgh (2009)
 *  Kevin Stratford (kevin@epcc.ed.ac.uk)
 *
 *****************************************************************************/

#ifndef SOBOL_POLY_H
#define SOBOL_POLY_H

poly_[0] = 1;
poly_[1] = 3;
poly_[2] = 7;
poly_[3] = 11;
poly_[4] = 13;
poly_[5] = 19;
poly_[6] = 25;
poly_[7] = 37;
poly_[8] = 59;
poly_[9] = 47;
poly_[10] = 61;
poly_[11] = 55;
poly_[12] = 41;
poly_[13] = 67;
poly_[14] = 97;
poly_[15] = 91;
poly_[16] = 109;
poly_[17] = 103;
poly_[18] = 115;
poly_[19] = 131;
poly_[20] = 193;
poly_[21] = 137;
poly_[22] = 145;
poly_[23] = 143;
poly_[24] = 241;
poly_[25] = 157;
poly_[26] = 185;
poly_[27] = 167;
poly_[28] = 229;
poly_[29] = 171;
poly_[30] = 213;
poly_[31] = 191;
poly_[32] = 253;
poly_[33] = 203;
poly_[34] = 211;
poly_[35] = 239;
poly_[36] = 247;
poly_[37] = 285;
poly_[38] = 369;
poly_[39] = 299;
poly_[40] = 301;
poly_[41] = 333;
poly_[42] = 351;
poly_[43] = 355;
poly_[44] = 357;
poly_[45] = 361;
poly_[46] = 391;
poly_[47] = 397;
poly_[48] = 425;
poly_[49] = 451;
poly_[50] = 463;
poly_[51] = 487;
poly_[52] = 501;
poly_[53] = 529;
poly_[54] = 539;
poly_[55] = 545;
poly_[56] = 557;
poly_[57] = 563;
poly_[58] = 601;
poly_[59] = 607;
poly_[60] = 617;
poly_[61] = 623;
poly_[62] = 631;
poly_[63] = 637;
poly_[64] = 647;
poly_[65] = 661;
poly_[66] = 675;
poly_[67] = 677;
poly_[68] = 687;
poly_[69] = 695;
poly_[70] = 701;
poly_[71] = 719;
poly_[72] = 721;
poly_[73] = 731;
poly_[74] = 757;
poly_[75] = 761;
poly_[76] = 787;
poly_[77] = 789;
poly_[78] = 799;
poly_[79] = 803;
poly_[80] = 817;
poly_[81] = 827;
poly_[82] = 847;
poly_[83] = 859;
poly_[84] = 865;
poly_[85] = 875;
poly_[86] = 877;
poly_[87] = 883;
poly_[88] = 895;
poly_[89] = 901;
poly_[90] = 911;
poly_[91] = 949;
poly_[92] = 953;
poly_[93] = 967;
poly_[94] = 971;
poly_[95] = 973;
poly_[96] = 981;
poly_[97] = 985;
poly_[98] = 995;
poly_[99] = 1001;
poly_[100] = 1019;
poly_[101] = 1033;
poly_[102] = 1051;
poly_[103] = 1063;
poly_[104] = 1069;
poly_[105] = 1125;
poly_[106] = 1135;
poly_[107] = 1153;
poly_[108] = 1163;
poly_[109] = 1221;
poly_[110] = 1239;
poly_[111] = 1255;
poly_[112] = 1267;
poly_[113] = 1279;
poly_[114] = 1293;
poly_[115] = 1305;
poly_[116] = 1315;
poly_[117] = 1329;
poly_[118] = 1341;
poly_[119] = 1347;
poly_[120] = 1367;
poly_[121] = 1387;
poly_[122] = 1413;
poly_[123] = 1423;
poly_[124] = 1431;
poly_[125] = 1441;
poly_[126] = 1479;
poly_[127] = 1509;
poly_[128] = 1527;
poly_[129] = 1531;
poly_[130] = 1555;
poly_[131] = 1557;
poly_[132] = 1573;
poly_[133] = 1591;
poly_[134] = 1603;
poly_[135] = 1615;
poly_[136] = 1627;
poly_[137] = 1657;
poly_[138] = 1663;
poly_[139] = 1673;
poly_[140] = 1717;
poly_[141] = 1729;
poly_[142] = 1747;
poly_[143] = 1759;
poly_[144] = 1789;
poly_[145] = 1815;
poly_[146] = 1821;
poly_[147] = 1825;
poly_[148] = 1849;
poly_[149] = 1863;
poly_[150] = 1869;
poly_[151] = 1877;
poly_[152] = 1881;
poly_[153] = 1891;
poly_[154] = 1917;
poly_[155] = 1933;
poly_[156] = 1939;
poly_[157] = 1969;
poly_[158] = 2011;
poly_[159] = 2035;
poly_[160] = 2041;
poly_[161] = 2053;
poly_[162] = 2071;
poly_[163] = 2091;
poly_[164] = 2093;
poly_[165] = 2119;
poly_[166] = 2147;
poly_[167] = 2149;
poly_[168] = 2161;
poly_[169] = 2171;
poly_[170] = 2189;
poly_[171] = 2197;
poly_[172] = 2207;
poly_[173] = 2217;
poly_[174] = 2225;
poly_[175] = 2255;
poly_[176] = 2257;
poly_[177] = 2273;
poly_[178] = 2279;
poly_[179] = 2283;
poly_[180] = 2293;
poly_[181] = 2317;
poly_[182] = 2323;
poly_[183] = 2341;
poly_[184] = 2345;
poly_[185] = 2363;
poly_[186] = 2365;
poly_[187] = 2373;
poly_[188] = 2377;
poly_[189] = 2385;
poly_[190] = 2395;
poly_[191] = 2419;
poly_[192] = 2421;
poly_[193] = 2431;
poly_[194] = 2435;
poly_[195] = 2447;
poly_[196] = 2475;
poly_[197] = 2477;
poly_[198] = 2489;
poly_[199] = 2503;
poly_[200] = 2521;
poly_[201] = 2533;
poly_[202] = 2551;
poly_[203] = 2561;
poly_[204] = 2567;
poly_[205] = 2579;
poly_[206] = 2581;
poly_[207] = 2601;
poly_[208] = 2633;
poly_[209] = 2657;
poly_[210] = 2669;
poly_[211] = 2681;
poly_[212] = 2687;
poly_[213] = 2693;
poly_[214] = 2705;
poly_[215] = 2717;
poly_[216] = 2727;
poly_[217] = 2731;
poly_[218] = 2739;
poly_[219] = 2741;
poly_[220] = 2773;
poly_[221] = 2783;
poly_[222] = 2793;
poly_[223] = 2799;
poly_[224] = 2801;
poly_[225] = 2811;
poly_[226] = 2819;
poly_[227] = 2825;
poly_[228] = 2833;
poly_[229] = 2867;
poly_[230] = 2879;
poly_[231] = 2881;
poly_[232] = 2891;
poly_[233] = 2905;
poly_[234] = 2911;
poly_[235] = 2917;
poly_[236] = 2927;
poly_[237] = 2941;
poly_[238] = 2951;
poly_[239] = 2955;
poly_[240] = 2963;
poly_[241] = 2965;
poly_[242] = 2991;
poly_[243] = 2999;
poly_[244] = 3005;
poly_[245] = 3017;
poly_[246] = 3035;
poly_[247] = 3037;
poly_[248] = 3047;
poly_[249] = 3053;
poly_[250] = 3083;
poly_[251] = 3085;
poly_[252] = 3097;
poly_[253] = 3103;
poly_[254] = 3159;
poly_[255] = 3169;
poly_[256] = 3179;
poly_[257] = 3187;
poly_[258] = 3205;
poly_[259] = 3209;
poly_[260] = 3223;
poly_[261] = 3227;
poly_[262] = 3229;
poly_[263] = 3251;
poly_[264] = 3263;
poly_[265] = 3271;
poly_[266] = 3277;
poly_[267] = 3283;
poly_[268] = 3285;
poly_[269] = 3299;
poly_[270] = 3305;
poly_[271] = 3319;
poly_[272] = 3331;
poly_[273] = 3343;
poly_[274] = 3357;
poly_[275] = 3367;
poly_[276] = 3373;
poly_[277] = 3393;
poly_[278] = 3399;
poly_[279] = 3413;
poly_[280] = 3417;
poly_[281] = 3427;
poly_[282] = 3439;
poly_[283] = 3441;
poly_[284] = 3475;
poly_[285] = 3487;
poly_[286] = 3497;
poly_[287] = 3515;
poly_[288] = 3517;
poly_[289] = 3529;
poly_[290] = 3543;
poly_[291] = 3547;
poly_[292] = 3553;
poly_[293] = 3559;
poly_[294] = 3573;
poly_[295] = 3589;
poly_[296] = 3613;
poly_[297] = 3617;
poly_[298] = 3623;
poly_[299] = 3627;
poly_[300] = 3635;
poly_[301] = 3641;
poly_[302] = 3655;
poly_[303] = 3659;
poly_[304] = 3669;
poly_[305] = 3679;
poly_[306] = 3697;
poly_[307] = 3707;
poly_[308] = 3709;
poly_[309] = 3713;
poly_[310] = 3731;
poly_[311] = 3743;
poly_[312] = 3747;
poly_[313] = 3771;
poly_[314] = 3791;
poly_[315] = 3805;
poly_[316] = 3827;
poly_[317] = 3833;
poly_[318] = 3851;
poly_[319] = 3865;
poly_[320] = 3889;
poly_[321] = 3895;
poly_[322] = 3933;
poly_[323] = 3947;
poly_[324] = 3949;
poly_[325] = 3957;
poly_[326] = 3971;
poly_[327] = 3985;
poly_[328] = 3991;
poly_[329] = 3995;
poly_[330] = 4007;
poly_[331] = 4013;
poly_[332] = 4021;
poly_[333] = 4045;
poly_[334] = 4051;
poly_[335] = 4069;
poly_[336] = 4073;
poly_[337] = 4179;
poly_[338] = 4201;
poly_[339] = 4219;
poly_[340] = 4221;
poly_[341] = 4249;
poly_[342] = 4305;
poly_[343] = 4331;
poly_[344] = 4359;
poly_[345] = 4383;
poly_[346] = 4387;
poly_[347] = 4411;
poly_[348] = 4431;
poly_[349] = 4439;
poly_[350] = 4449;
poly_[351] = 4459;
poly_[352] = 4485;
poly_[353] = 4531;
poly_[354] = 4569;
poly_[355] = 4575;
poly_[356] = 4621;
poly_[357] = 4663;
poly_[358] = 4669;
poly_[359] = 4711;
poly_[360] = 4723;
poly_[361] = 4735;
poly_[362] = 4793;
poly_[363] = 4801;
poly_[364] = 4811;
poly_[365] = 4879;
poly_[366] = 4893;
poly_[367] = 4897;
poly_[368] = 4921;
poly_[369] = 4927;
poly_[370] = 4941;
poly_[371] = 4977;
poly_[372] = 5017;
poly_[373] = 5027;
poly_[374] = 5033;
poly_[375] = 5127;
poly_[376] = 5169;
poly_[377] = 5175;
poly_[378] = 5199;
poly_[379] = 5213;
poly_[380] = 5223;
poly_[381] = 5237;
poly_[382] = 5287;
poly_[383] = 5293;
poly_[384] = 5331;
poly_[385] = 5391;
poly_[386] = 5405;
poly_[387] = 5453;
poly_[388] = 5523;
poly_[389] = 5573;
poly_[390] = 5591;
poly_[391] = 5597;
poly_[392] = 5611;
poly_[393] = 5641;
poly_[394] = 5703;
poly_[395] = 5717;
poly_[396] = 5721;
poly_[397] = 5797;
poly_[398] = 5821;
poly_[399] = 5909;
poly_[400] = 5913;
poly_[401] = 5955;
poly_[402] = 5957;
poly_[403] = 6005;
poly_[404] = 6025;
poly_[405] = 6061;
poly_[406] = 6067;
poly_[407] = 6079;
poly_[408] = 6081;
poly_[409] = 6231;
poly_[410] = 6237;
poly_[411] = 6289;
poly_[412] = 6295;
poly_[413] = 6329;
poly_[414] = 6383;
poly_[415] = 6427;
poly_[416] = 6453;
poly_[417] = 6465;
poly_[418] = 6501;
poly_[419] = 6523;
poly_[420] = 6539;
poly_[421] = 6577;
poly_[422] = 6589;
poly_[423] = 6601;
poly_[424] = 6607;
poly_[425] = 6631;
poly_[426] = 6683;
poly_[427] = 6699;
poly_[428] = 6707;
poly_[429] = 6761;
poly_[430] = 6795;
poly_[431] = 6865;
poly_[432] = 6881;
poly_[433] = 6901;
poly_[434] = 6923;
poly_[435] = 6931;
poly_[436] = 6943;
poly_[437] = 6999;
poly_[438] = 7057;
poly_[439] = 7079;
poly_[440] = 7103;
poly_[441] = 7105;
poly_[442] = 7123;
poly_[443] = 7173;
poly_[444] = 7185;
poly_[445] = 7191;
poly_[446] = 7207;
poly_[447] = 7245;
poly_[448] = 7303;
poly_[449] = 7327;
poly_[450] = 7333;
poly_[451] = 7355;
poly_[452] = 7365;
poly_[453] = 7369;
poly_[454] = 7375;
poly_[455] = 7411;
poly_[456] = 7431;
poly_[457] = 7459;
poly_[458] = 7491;
poly_[459] = 7505;
poly_[460] = 7515;
poly_[461] = 7541;
poly_[462] = 7557;
poly_[463] = 7561;
poly_[464] = 7701;
poly_[465] = 7705;
poly_[466] = 7727;
poly_[467] = 7749;
poly_[468] = 7761;
poly_[469] = 7783;
poly_[470] = 7795;
poly_[471] = 7823;
poly_[472] = 7907;
poly_[473] = 7953;
poly_[474] = 7963;
poly_[475] = 7975;
poly_[476] = 8049;
poly_[477] = 8089;
poly_[478] = 8123;
poly_[479] = 8125;
poly_[480] = 8137;
poly_[481] = 8219;
poly_[482] = 8231;
poly_[483] = 8245;
poly_[484] = 8275;
poly_[485] = 8293;
poly_[486] = 8303;
poly_[487] = 8331;
poly_[488] = 8333;
poly_[489] = 8351;
poly_[490] = 8357;
poly_[491] = 8367;
poly_[492] = 8379;
poly_[493] = 8381;
poly_[494] = 8387;
poly_[495] = 8393;
poly_[496] = 8417;
poly_[497] = 8435;
poly_[498] = 8461;
poly_[499] = 8469;
poly_[500] = 8489;
poly_[501] = 8495;
poly_[502] = 8507;
poly_[503] = 8515;
poly_[504] = 8551;
poly_[505] = 8555;
poly_[506] = 8569;
poly_[507] = 8585;
poly_[508] = 8599;
poly_[509] = 8605;
poly_[510] = 8639;
poly_[511] = 8641;
poly_[512] = 8647;
poly_[513] = 8653;
poly_[514] = 8671;
poly_[515] = 8675;
poly_[516] = 8689;
poly_[517] = 8699;
poly_[518] = 8729;
poly_[519] = 8741;
poly_[520] = 8759;
poly_[521] = 8765;
poly_[522] = 8771;
poly_[523] = 8795;
poly_[524] = 8797;
poly_[525] = 8825;
poly_[526] = 8831;
poly_[527] = 8841;
poly_[528] = 8855;
poly_[529] = 8859;
poly_[530] = 8883;
poly_[531] = 8895;
poly_[532] = 8909;
poly_[533] = 8943;
poly_[534] = 8951;
poly_[535] = 8955;
poly_[536] = 8965;
poly_[537] = 8999;
poly_[538] = 9003;
poly_[539] = 9031;
poly_[540] = 9045;
poly_[541] = 9049;
poly_[542] = 9071;
poly_[543] = 9073;
poly_[544] = 9085;
poly_[545] = 9095;
poly_[546] = 9101;
poly_[547] = 9109;
poly_[548] = 9123;
poly_[549] = 9129;
poly_[550] = 9137;
poly_[551] = 9143;
poly_[552] = 9147;
poly_[553] = 9185;
poly_[554] = 9197;
poly_[555] = 9209;
poly_[556] = 9227;
poly_[557] = 9235;
poly_[558] = 9247;
poly_[559] = 9253;
poly_[560] = 9257;
poly_[561] = 9277;
poly_[562] = 9297;
poly_[563] = 9303;
poly_[564] = 9313;
poly_[565] = 9325;
poly_[566] = 9343;
poly_[567] = 9347;
poly_[568] = 9371;
poly_[569] = 9373;
poly_[570] = 9397;
poly_[571] = 9407;
poly_[572] = 9409;
poly_[573] = 9415;
poly_[574] = 9419;
poly_[575] = 9443;
poly_[576] = 9481;
poly_[577] = 9495;
poly_[578] = 9501;
poly_[579] = 9505;
poly_[580] = 9517;
poly_[581] = 9529;
poly_[582] = 9555;
poly_[583] = 9557;
poly_[584] = 9571;
poly_[585] = 9585;
poly_[586] = 9591;
poly_[587] = 9607;
poly_[588] = 9611;
poly_[589] = 9621;
poly_[590] = 9625;
poly_[591] = 9631;
poly_[592] = 9647;
poly_[593] = 9661;
poly_[594] = 9669;
poly_[595] = 9679;
poly_[596] = 9687;
poly_[597] = 9707;
poly_[598] = 9731;
poly_[599] = 9733;
poly_[600] = 9745;
poly_[601] = 9773;
poly_[602] = 9791;
poly_[603] = 9803;
poly_[604] = 9811;
poly_[605] = 9817;
poly_[606] = 9833;
poly_[607] = 9847;
poly_[608] = 9851;
poly_[609] = 9863;
poly_[610] = 9875;
poly_[611] = 9881;
poly_[612] = 9905;
poly_[613] = 9911;
poly_[614] = 9917;
poly_[615] = 9923;
poly_[616] = 9963;
poly_[617] = 9973;
poly_[618] = 10003;
poly_[619] = 10025;
poly_[620] = 10043;
poly_[621] = 10063;
poly_[622] = 10071;
poly_[623] = 10077;
poly_[624] = 10091;
poly_[625] = 10099;
poly_[626] = 10105;
poly_[627] = 10115;
poly_[628] = 10129;
poly_[629] = 10145;
poly_[630] = 10169;
poly_[631] = 10183;
poly_[632] = 10187;
poly_[633] = 10207;
poly_[634] = 10223;
poly_[635] = 10225;
poly_[636] = 10247;
poly_[637] = 10265;
poly_[638] = 10271;
poly_[639] = 10275;
poly_[640] = 10289;
poly_[641] = 10299;
poly_[642] = 10301;
poly_[643] = 10309;
poly_[644] = 10343;
poly_[645] = 10357;
poly_[646] = 10373;
poly_[647] = 10411;
poly_[648] = 10413;
poly_[649] = 10431;
poly_[650] = 10445;
poly_[651] = 10453;
poly_[652] = 10463;
poly_[653] = 10467;
poly_[654] = 10473;
poly_[655] = 10491;
poly_[656] = 10505;
poly_[657] = 10511;
poly_[658] = 10513;
poly_[659] = 10523;
poly_[660] = 10539;
poly_[661] = 10549;
poly_[662] = 10559;
poly_[663] = 10561;
poly_[664] = 10571;
poly_[665] = 10581;
poly_[666] = 10615;
poly_[667] = 10621;
poly_[668] = 10625;
poly_[669] = 10643;
poly_[670] = 10655;
poly_[671] = 10671;
poly_[672] = 10679;
poly_[673] = 10685;
poly_[674] = 10691;
poly_[675] = 10711;
poly_[676] = 10739;
poly_[677] = 10741;
poly_[678] = 10755;
poly_[679] = 10767;
poly_[680] = 10781;
poly_[681] = 10785;
poly_[682] = 10803;
poly_[683] = 10805;
poly_[684] = 10829;
poly_[685] = 10857;
poly_[686] = 10863;
poly_[687] = 10865;
poly_[688] = 10875;
poly_[689] = 10877;
poly_[690] = 10917;
poly_[691] = 10921;
poly_[692] = 10929;
poly_[693] = 10949;
poly_[694] = 10967;
poly_[695] = 10971;
poly_[696] = 10987;
poly_[697] = 10995;
poly_[698] = 11009;
poly_[699] = 11029;
poly_[700] = 11043;
poly_[701] = 11045;
poly_[702] = 11055;
poly_[703] = 11063;
poly_[704] = 11075;
poly_[705] = 11081;
poly_[706] = 11117;
poly_[707] = 11135;
poly_[708] = 11141;
poly_[709] = 11159;
poly_[710] = 11163;
poly_[711] = 11181;
poly_[712] = 11187;
poly_[713] = 11225;
poly_[714] = 11237;
poly_[715] = 11261;
poly_[716] = 11279;
poly_[717] = 11297;
poly_[718] = 11307;
poly_[719] = 11309;
poly_[720] = 11327;
poly_[721] = 11329;
poly_[722] = 11341;
poly_[723] = 11377;
poly_[724] = 11403;
poly_[725] = 11405;
poly_[726] = 11413;
poly_[727] = 11427;
poly_[728] = 11439;
poly_[729] = 11453;
poly_[730] = 11461;
poly_[731] = 11473;
poly_[732] = 11479;
poly_[733] = 11489;
poly_[734] = 11495;
poly_[735] = 11499;
poly_[736] = 11533;
poly_[737] = 11545;
poly_[738] = 11561;
poly_[739] = 11567;
poly_[740] = 11575;
poly_[741] = 11579;
poly_[742] = 11589;
poly_[743] = 11611;
poly_[744] = 11623;
poly_[745] = 11637;
poly_[746] = 11657;
poly_[747] = 11663;
poly_[748] = 11687;
poly_[749] = 11691;
poly_[750] = 11701;
poly_[751] = 11747;
poly_[752] = 11761;
poly_[753] = 11773;
poly_[754] = 11783;
poly_[755] = 11795;
poly_[756] = 11797;
poly_[757] = 11817;
poly_[758] = 11849;
poly_[759] = 11855;
poly_[760] = 11867;
poly_[761] = 11869;
poly_[762] = 11873;
poly_[763] = 11883;
poly_[764] = 11919;
poly_[765] = 11921;
poly_[766] = 11927;
poly_[767] = 11933;
poly_[768] = 11947;
poly_[769] = 11955;
poly_[770] = 11961;
poly_[771] = 11999;
poly_[772] = 12027;
poly_[773] = 12029;
poly_[774] = 12037;
poly_[775] = 12041;
poly_[776] = 12049;
poly_[777] = 12055;
poly_[778] = 12095;
poly_[779] = 12097;
poly_[780] = 12107;
poly_[781] = 12109;
poly_[782] = 12121;
poly_[783] = 12127;
poly_[784] = 12133;
poly_[785] = 12137;
poly_[786] = 12181;
poly_[787] = 12197;
poly_[788] = 12207;
poly_[789] = 12209;
poly_[790] = 12239;
poly_[791] = 12253;
poly_[792] = 12263;
poly_[793] = 12269;
poly_[794] = 12277;
poly_[795] = 12287;
poly_[796] = 12295;
poly_[797] = 12309;
poly_[798] = 12313;
poly_[799] = 12335;
poly_[800] = 12361;
poly_[801] = 12367;
poly_[802] = 12391;
poly_[803] = 12409;
poly_[804] = 12415;
poly_[805] = 12433;
poly_[806] = 12449;
poly_[807] = 12469;
poly_[808] = 12479;
poly_[809] = 12481;
poly_[810] = 12499;
poly_[811] = 12505;
poly_[812] = 12517;
poly_[813] = 12527;
poly_[814] = 12549;
poly_[815] = 12559;
poly_[816] = 12597;
poly_[817] = 12615;
poly_[818] = 12621;
poly_[819] = 12639;
poly_[820] = 12643;
poly_[821] = 12657;
poly_[822] = 12667;
poly_[823] = 12707;
poly_[824] = 12713;
poly_[825] = 12727;
poly_[826] = 12741;
poly_[827] = 12745;
poly_[828] = 12763;
poly_[829] = 12769;
poly_[830] = 12779;
poly_[831] = 12781;
poly_[832] = 12787;
poly_[833] = 12799;
poly_[834] = 12809;
poly_[835] = 12815;
poly_[836] = 12829;
poly_[837] = 12839;
poly_[838] = 12857;
poly_[839] = 12875;
poly_[840] = 12883;
poly_[841] = 12889;
poly_[842] = 12901;
poly_[843] = 12929;
poly_[844] = 12947;
poly_[845] = 12953;
poly_[846] = 12959;
poly_[847] = 12969;
poly_[848] = 12983;
poly_[849] = 12987;
poly_[850] = 12995;
poly_[851] = 13015;
poly_[852] = 13019;
poly_[853] = 13031;
poly_[854] = 13063;
poly_[855] = 13077;
poly_[856] = 13103;
poly_[857] = 13137;
poly_[858] = 13149;
poly_[859] = 13173;
poly_[860] = 13207;
poly_[861] = 13211;
poly_[862] = 13227;
poly_[863] = 13241;
poly_[864] = 13249;
poly_[865] = 13255;
poly_[866] = 13269;
poly_[867] = 13283;
poly_[868] = 13285;
poly_[869] = 13303;
poly_[870] = 13307;
poly_[871] = 13321;
poly_[872] = 13339;
poly_[873] = 13351;
poly_[874] = 13377;
poly_[875] = 13389;
poly_[876] = 13407;
poly_[877] = 13417;
poly_[878] = 13431;
poly_[879] = 13435;
poly_[880] = 13447;
poly_[881] = 13459;
poly_[882] = 13465;
poly_[883] = 13477;
poly_[884] = 13501;
poly_[885] = 13513;
poly_[886] = 13531;
poly_[887] = 13543;
poly_[888] = 13561;
poly_[889] = 13581;
poly_[890] = 13599;
poly_[891] = 13605;
poly_[892] = 13617;
poly_[893] = 13623;
poly_[894] = 13637;
poly_[895] = 13647;
poly_[896] = 13661;
poly_[897] = 13677;
poly_[898] = 13683;
poly_[899] = 13695;
poly_[900] = 13725;
poly_[901] = 13729;
poly_[902] = 13753;
poly_[903] = 13773;
poly_[904] = 13781;
poly_[905] = 13785;
poly_[906] = 13795;
poly_[907] = 13801;
poly_[908] = 13807;
poly_[909] = 13825;
poly_[910] = 13835;
poly_[911] = 13855;
poly_[912] = 13861;
poly_[913] = 13871;
poly_[914] = 13883;
poly_[915] = 13897;
poly_[916] = 13905;
poly_[917] = 13915;
poly_[918] = 13939;
poly_[919] = 13941;
poly_[920] = 13969;
poly_[921] = 13979;
poly_[922] = 13981;
poly_[923] = 13997;
poly_[924] = 14027;
poly_[925] = 14035;
poly_[926] = 14037;
poly_[927] = 14051;
poly_[928] = 14063;
poly_[929] = 14085;
poly_[930] = 14095;
poly_[931] = 14107;
poly_[932] = 14113;
poly_[933] = 14125;
poly_[934] = 14137;
poly_[935] = 14145;
poly_[936] = 14151;
poly_[937] = 14163;
poly_[938] = 14193;
poly_[939] = 14199;
poly_[940] = 14219;
poly_[941] = 14229;
poly_[942] = 14233;
poly_[943] = 14243;
poly_[944] = 14277;
poly_[945] = 14287;
poly_[946] = 14289;
poly_[947] = 14295;
poly_[948] = 14301;
poly_[949] = 14305;
poly_[950] = 14323;
poly_[951] = 14339;
poly_[952] = 14341;
poly_[953] = 14359;
poly_[954] = 14365;
poly_[955] = 14375;
poly_[956] = 14387;
poly_[957] = 14411;
poly_[958] = 14425;
poly_[959] = 14441;
poly_[960] = 14449;
poly_[961] = 14499;
poly_[962] = 14513;
poly_[963] = 14523;
poly_[964] = 14537;
poly_[965] = 14543;
poly_[966] = 14561;
poly_[967] = 14579;
poly_[968] = 14585;
poly_[969] = 14593;
poly_[970] = 14599;
poly_[971] = 14603;
poly_[972] = 14611;
poly_[973] = 14641;
poly_[974] = 14671;
poly_[975] = 14695;
poly_[976] = 14701;
poly_[977] = 14723;
poly_[978] = 14725;
poly_[979] = 14743;
poly_[980] = 14753;
poly_[981] = 14759;
poly_[982] = 14765;
poly_[983] = 14795;
poly_[984] = 14797;
poly_[985] = 14803;
poly_[986] = 14831;
poly_[987] = 14839;
poly_[988] = 14845;
poly_[989] = 14855;
poly_[990] = 14889;
poly_[991] = 14895;
poly_[992] = 14909;
poly_[993] = 14929;
poly_[994] = 14941;
poly_[995] = 14945;
poly_[996] = 14951;
poly_[997] = 14963;
poly_[998] = 14965;
poly_[999] = 14985;
poly_[1000] = 15033;
poly_[1001] = 15039;
poly_[1002] = 15053;
poly_[1003] = 15059;
poly_[1004] = 15061;
poly_[1005] = 15071;
poly_[1006] = 15077;
poly_[1007] = 15081;
poly_[1008] = 15099;
poly_[1009] = 15121;
poly_[1010] = 15147;
poly_[1011] = 15149;
poly_[1012] = 15157;
poly_[1013] = 15167;
poly_[1014] = 15187;
poly_[1015] = 15193;
poly_[1016] = 15203;
poly_[1017] = 15205;
poly_[1018] = 15215;
poly_[1019] = 15217;
poly_[1020] = 15223;
poly_[1021] = 15243;
poly_[1022] = 15257;
poly_[1023] = 15269;
poly_[1024] = 15273;
poly_[1025] = 15287;
poly_[1026] = 15291;
poly_[1027] = 15313;
poly_[1028] = 15335;
poly_[1029] = 15347;
poly_[1030] = 15359;
poly_[1031] = 15373;
poly_[1032] = 15379;
poly_[1033] = 15381;
poly_[1034] = 15391;
poly_[1035] = 15395;
poly_[1036] = 15397;
poly_[1037] = 15419;
poly_[1038] = 15439;
poly_[1039] = 15453;
poly_[1040] = 15469;
poly_[1041] = 15491;
poly_[1042] = 15503;
poly_[1043] = 15517;
poly_[1044] = 15527;
poly_[1045] = 15531;
poly_[1046] = 15545;
poly_[1047] = 15559;
poly_[1048] = 15593;
poly_[1049] = 15611;
poly_[1050] = 15613;
poly_[1051] = 15619;
poly_[1052] = 15639;
poly_[1053] = 15643;
poly_[1054] = 15649;
poly_[1055] = 15661;
poly_[1056] = 15667;
poly_[1057] = 15669;
poly_[1058] = 15681;
poly_[1059] = 15693;
poly_[1060] = 15717;
poly_[1061] = 15721;
poly_[1062] = 15741;
poly_[1063] = 15745;
poly_[1064] = 15765;
poly_[1065] = 15793;
poly_[1066] = 15799;
poly_[1067] = 15811;
poly_[1068] = 15825;
poly_[1069] = 15835;
poly_[1070] = 15847;
poly_[1071] = 15851;
poly_[1072] = 15865;
poly_[1073] = 15877;
poly_[1074] = 15881;
poly_[1075] = 15887;
poly_[1076] = 15899;
poly_[1077] = 15915;
poly_[1078] = 15935;
poly_[1079] = 15937;
poly_[1080] = 15955;
poly_[1081] = 15973;
poly_[1082] = 15977;
poly_[1083] = 16011;
poly_[1084] = 16035;
poly_[1085] = 16061;
poly_[1086] = 16069;
poly_[1087] = 16087;
poly_[1088] = 16093;
poly_[1089] = 16097;
poly_[1090] = 16121;
poly_[1091] = 16141;
poly_[1092] = 16153;
poly_[1093] = 16159;
poly_[1094] = 16165;
poly_[1095] = 16183;
poly_[1096] = 16189;
poly_[1097] = 16195;
poly_[1098] = 16197;
poly_[1099] = 16201;
poly_[1100] = 16209;
poly_[1101] = 16215;
poly_[1102] = 16225;
poly_[1103] = 16259;
poly_[1104] = 16265;
poly_[1105] = 16273;
poly_[1106] = 16299;
poly_[1107] = 16309;
poly_[1108] = 16355;
poly_[1109] = 16375;
poly_[1110] = 16381;

#endif
