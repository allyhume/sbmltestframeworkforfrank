/********************************************************************/
/*!
  @file SBMLModel.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.3 $

*/
/******************************************************************/
using namespace std;

#ifndef __SBMLModel_h__
#define __SBMLModel_h__

/**
  Overrides cModel and provides empty implementations of getRHS and calVar().
  Okay buy why? (comment added by adc).
 */
class SBMLModel : public cModel
{
public:
  SBMLModel() {};

  virtual ~SBMLModel() {};
  virtual void inputModel() {};

  virtual void getRHS(double* y_in, double t, double* yout) {};
  virtual void update_assignment_variables(
                  vector< vector<double> > *results) {};


  virtual vector<double> calVarVec(int iv, vector<vector<double> > ) {};

  /**
   * Function invoked by ModelCostFunction after setup of parameter valuse and before initialisation of solver.
   * Sutable for recalculations of initial states based upon parameter values
   */

  virtual void calCoef() {} ;

  virtual void setBoundary() {};
  virtual void setInitialGuess() {};
};

#endif
