/********************************************************************/
/*!
  @file PGAHibrid.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.1 $

*/
/******************************************************************/

#ifndef __PGAHibrid_h__
#define __PGAHibrid_h__

#include <Optimiser.h>
#include <PGSWrapper.h>

using namespace std;

class PGAHybrid: public PGSWrapper
{
public:
  PGAHybrid() {
    fixedSeed = 0;
  }
protected:

};

#endif
