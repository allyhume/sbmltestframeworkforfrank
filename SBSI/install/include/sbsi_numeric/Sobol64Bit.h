/*****************************************************************************
 *
 *  Sobol64Bit.h
 *
 *  Generator of Sobol low-discrepancy (aka 'quasi-random') sequences
 *  (64-bit version). Although the 64-bit only really refers to the
 *  seed, all the integers are declared 'long long int' for consistency.
 *
 *  See Joe and Kuo, ACM TOMS 29, 49--57 (2003) and references
 *  therein for a complete description.
 *
 *  This version is adapted from the Fortran implementation of
 *  John Burkardt available from
 *
 *  http://www.scs.fsu.edu/~burkardt/cpp.src/sobol/sobol.html
 *
 *  There are two accompanying files with contain tables of
 *     - the primitive polynomials (SobolPolynomials.h)
 *     - the freely chosen direction numbers (SobolDirectionNumbers.h)
 *
 *  These are merely to avoid the inconvenience of huge tables
 *  in the source code.
 *
 *  $Id: Sobol64Bit.h,v 1.1.2.1 2010/05/20 14:36:27 ayamaguc Exp $
 *
 *  Millar Laboratory, Centre for Systems Biology at Edinburgh
 *  (c) The University of Edinburgh (2009)
 *  Kevin Stratford (kevin@epcc.ed.ac.uk)
 *
 *****************************************************************************/

#include <vector>

#ifndef SOBOL64BIT_H
#define SOBOL64BIT_H

#define SOBOL_DIM_MAX 1111
#define SOBOL_LOG_MAX 62

class Sobol64Bit
{

public:
  Sobol64Bit(const long long int dimension);
  ~Sobol64Bit();

  void reap(double * sequence);
  void reap(std::vector<double> &sequence);
  void setSeed(const long long int seed);
  long long int getSeed();

  long long int lowZeroBit(long long int) const;

private:
  long long int dimension_;                        // dimensions
  long long int seed_;                             // current seed
  long long int v_[SOBOL_LOG_MAX][SOBOL_DIM_MAX];  // direction numbers
  long long int poly_[SOBOL_DIM_MAX];              // primitive polynomials
  long long int lastq_[SOBOL_DIM_MAX];             // current state
  long long int maxseed_;                          // maximum seed value
  double rnormalise_;                              // normaliser

  void initialisePolynomials(void);
  void initialiseDirectionNumbers(void);
  void recurrenceRelation(void);
};

#endif
