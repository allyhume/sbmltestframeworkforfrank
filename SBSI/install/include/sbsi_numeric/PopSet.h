/********************************************************************/
/*!
  @file PopSet.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.1 $

*/
/******************************************************************/

using namespace std;

#ifndef __PopSet_h__
#define __PopSet_h__

class PopSet
{
public:
  vector<string> g_info;
  vector<double> params;

  friend ostream &operator << (ostream &stream, PopSet &ps);
};
#endif



