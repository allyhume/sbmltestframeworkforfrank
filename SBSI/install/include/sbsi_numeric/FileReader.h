/********************************************************************/
/*!
  @file FileReader.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.1 $

*/
/******************************************************************/
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

#ifndef __FileReader_h__
#define __FileReader_h__

#include <DataSet.h>
#include <PopSet.h>
//#include <cModelArg.h>
#include <ParamInfo.h>

enum FileReaderType {
  TabDelimitedColumnData,
  TabDelimitedColumnPop,
  TabDelimitedColumnParamInfo,
  TabDelimitedColumnHeader
};

class FileReader
{

public:

  virtual ~FileReader() {};

  vector<DataSet> Sets;

  DataSet *dataset;

  vector<PopSet> PopSets;

  ParamInfo paraminfo;


  //cModelArg *ModelArg;


  virtual void Read(string file) = 0;

  virtual void ClearSets() {
    Sets.clear();
  }

  //virtual void SetModelArg(cModelArg &ma) {ModelArg = &ma;} // for ParamInfo

  virtual void ClearParamInfo() {
    paraminfo.Clear();
  };

  virtual void SetDataSet(DataSet *ds) {
    dataset = ds;
  }

  static FileReader & SetFileReader(FileReaderType which);

  ////////////////////////////////////////////////////
  // Utilities
  ////////////////////////////////////////////////////

  string GetLine(fstream &f);

  vector<string> Tokenize(string line);

  bool IsDelimiter(char c);

  bool IsWhitespace(char c);

  bool IsWord(string s);

  bool IsNum(string s);

  double ToDouble(string s);

  string StripWhitespace(string);

  void SetTokenDelimiters(string chars);

  void SetWhitespaces(string chars);

  string delimiters;
  string whitespaces;


};

class TDCFileReader: public FileReader
{
public:
  virtual void Read(string file);
};

class MltTDCFileReader: public FileReader
{
public:
  virtual void Read(string file);
};

class TDCPopFileReader: public FileReader
{
public:
  virtual void Read(string file);
};

class TDCParamInfoFileReader: public FileReader
{
public:
  virtual void Read(string file);
};

class TDCHeaderFileReader: public FileReader
{
public:
  virtual void Read(string file);
};
#endif

