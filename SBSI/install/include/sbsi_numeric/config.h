/* include/config.h.  Generated from config.h.in by configure.  */
/* include/config.h.in.  Generated from configure.ac by autoheader.  */

/* Use fftw 2.1.5 */
/* #undef ENABLE_FFTW2 */

/* Define to 1 if you have the `cppunit' library (-lcppunit). */
#define HAVE_LIBCPPUNIT 1

/* Define to 1 if you have the `fftw' library (-lfftw). */
/* #undef HAVE_LIBFFTW */

/* Define to 1 if you have the `fftw3' library (-lfftw3). */
#define HAVE_LIBFFTW3 1

/* Define to 1 if you have the `pgapack' library (-lpgapack). */
#define HAVE_LIBPGAPACK 1

/* Define to 1 if you have the `sbml' library (-lsbml). */
#define HAVE_LIBSBML 1

/* Define to 1 if you have the `sundials_cvode' library (-lsundials_cvode). */
#define HAVE_LIBSUNDIALS_CVODE 1

/* Define to 1 if you have the `sundials_kinsol' library (-lsundials_kinsol).
   */
#define HAVE_LIBSUNDIALS_KINSOL 1

/* Define to 1 if you have the `sundials_nvecserial' library
   (-lsundials_nvecserial). */
#define HAVE_LIBSUNDIALS_NVECSERIAL 1

/* Name of package */
#define PACKAGE "sbsi_numeric"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "BUG-REPORT-ADDRESS"

/* Define to the full name of this package. */
#define PACKAGE_NAME "SBSI_NUMERIC"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "SBSI_NUMERIC 3.2"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "sbsi_numeric"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "3.2"

/* Version number of package */
#define VERSION "3.2"
