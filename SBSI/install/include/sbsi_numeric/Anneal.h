/********************************************************************/
/*!
  @file Anneal.h
  @brief Class for Simulated Annealing: Not A Parallel Code
  @date 2010/03/29
  @author Azusa Yamaguchi
  $Revision: 1.1.2.1 $

*/
/******************************************************************/
#ifndef __Anneal_h__
#define __Anneal_h__

#include <Optimiser.h>

using namespace std;

/*!
 * @brief Class for Simulated Annealing: Not A Parallel Code
 */
class Anneal : public Optimiser
{
public:

  /*!
    @brief  The high-level routine to execute Simulated annealing algorithm.
    @return The iterarion number
  */
  virtual int Run();

private:

  /*!
   @brief To calculate a cost value a each paramser p
   @param ParamSet p
   @return its cost value
  */
  double CalcEnergy(ParamSet p);

  /*!
   @brief To check the stop conditions, A) exceed Max iteration, B) exceed the max nochange or C)reached the target cost value
   @param iteration, a counter for max_no_change, the cuurent cost value, file output ofstream
  */
  void StopCondition(int iter, int count, double cost, ofstream &fp);

  /*!
   @brief To print the current population to the checkpoint directory file
   @param iteration
  */
  void PrintPop(int iter);
};

#endif


