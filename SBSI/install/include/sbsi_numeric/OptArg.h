/********************************************************************/
/*!
  @file OptArg.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.2 $

*/
/******************************************************************/

#include<cfloat>

#include<ERR.h>

#ifndef __OptArg_h__
#define __OptArg_h__

using std::string;

typedef enum {
  InvalidOptimiser,
  PGA,
  PGAPSO,
  Composite,
  DirectSearch
} OptimiserType;

typedef enum {
  InvalidSetup,
  SobolSelect,
  SobolUnselect,
  ReadinFile
} SetupType;

struct OptArg {
  // Lots of members are left with undefined values
  OptArg():
    optimisertype(InvalidOptimiser), setuptype(InvalidSetup) {
    PopSize = -1;
    NumBest = -1;
    NumGen = -1;
    PSONumGen = -1;
    Inertia = 0.729;
    Congnitive = 1.494;
    Social = 1.494;
    MaxSimilarity = -1;
    MaxNoChange = -1;
    CkpointStartNum = -1;
    RestartFreq = -1;
    MutProb = -1;
    Mu = -1;
    Seed = -1;
    SobolSeed = -1;
    Verbose = 0;
    PrintFreq = 0;
    CkpointFreq = 0;
    MINCOST = DBL_MIN;
    OutBestPopfile.clear();
    ResultDir.clear();
    CkpointDir.clear();
    PrintBestPop = true;
    MyMutation = NULL;
    MyCrossOver = NULL;
    MyEndOfGen = NULL;
  }

  // Sobol Seed
  long long int getSobolSeed() {
    return SobolSeed;
  };
  void  setSobolSeed(long long int seed) {
    SobolSeed = seed;
  };

  // return optimiser_type
  OptimiserType getOptimiserType()const {
    return optimisertype;
  };
  void setOptimiserType(OptimiserType ot) {
    optimisertype = ot;
  };

  // return setup_type
  SetupType getSetupType()const {
    return setuptype;
  };
  void setSetupType(SetupType ot) {
    setuptype = ot;
  };
  // PGA Seed
  int getSeed()const {
    return Seed;
  };
  void setSeed(int s) {
    Seed = s;
  };

  //return NumGen
  int getNumGen() const {
    if (NumGen <= 0)
      ERR.General("Optimiser", __func__,
                  "Invalid Generation number: %d which is <= 0\n", NumGen);
    return NumGen;
  };
  void setNumGen(int g) {
    NumGen = g;
  };

  //return PSONumGen
  int getPSONumGen()const {
    if (PSONumGen <= 0)
      ERR.General("Optimiser", __func__,
                  "Invalid PSO Generation Number: %d, which is <= 0 \n", PSONumGen);
    return PSONumGen;
  };
  void setPSONumGen(int g) {
    PSONumGen = g;
  };

  //return Inertia
  double getInertia()const {
    return Inertia;
  };
  void setInertia(double ir) {
    Inertia = ir;
  };

  //return Congnitive
  double getCongnitive()const {
    return Congnitive;
  };
  void setCongnitive(double ir) {
    Congnitive = ir;
  };

  //return Social
  double getSocial()const {
    return Social;
  };
  void setSocial(double ir) {
    Social = ir;
  };

  //return NumBest
  int getNumBest()const {
    if (NumBest < 0) {
      ERR.General("Optimiser", __func__, "Invalid the number of Best population to be passed to the next generation.\n");
    }
    return NumBest;
  };
  void setNumBest(int i) {
    NumBest = i;
  };

  //return PopSize
  int getPopSize()const {
    if (PopSize <= 0) {
      ERR.General("Optimiser", __func__, "Invalid Population size.; %d", PopSize);
    }
    return PopSize;
  };
  void setPopSize(int p) {
    PopSize = p;
  };

  //return MaxNoChange
  int getMaxNoChange()const {
    if (MaxNoChange <= 0) {
      ERR.General("Optimiser", __func__, "Invalid Maximum number for the stop crieteia \"NoImprovement\". " );
    }
    return MaxNoChange;
  };
  void setMaxNoChange(int i) {
    MaxNoChange = i;
  };

  //return MinCost
  double getMinCost()const {
    if (MINCOST <= DBL_MIN ) {
      ERR.General("Optimiser", __func__, "Invalid the target costfunction value.");
    }
    return MINCOST;
  };
  void setMinCost(double a) {
    MINCOST = a;
  };

  // return MaxSimilarity
  int getMaxSimilarity()const {
    if (MaxSimilarity <= 0 ) {
      ERR.General("Optimiser", __func__, "Invalid value for the maximun similarity ratio of a population.");
    }
    return MaxSimilarity;
  };
  void setMaxSimilarity(int i) {
    MaxSimilarity = i;
  };

  // return ckpointFreq
  int getCkpointFreq()const {
    if (CkpointFreq == 0 ) {
      ERR.General("Optimiser", __func__, "Invalid number for the check point frequency.");
    }
    return CkpointFreq;
  };
  void setCkpointFreq(int i) {
    CkpointFreq = i;
  };

  // return ckpointStartNum
  int getCkpointStartNum()const {
    return CkpointStartNum;
  };
  void setCkpointStartNum(int i) {
    CkpointStartNum = i;
  };

  // return PrintFreq
  int getPrintFreq()const {
    return PrintFreq;
  };
  void setPrintFreq(int i) {
    PrintFreq = i;
  };

  // return RestartFreq
  int getRestartFreq()const {
    return RestartFreq;
  };
  void setRestartFreq(int i) {
    RestartFreq = i;
  };

  // return mutPorb
  double getMutProb()const {
    return MutProb;
  };
  void setMutProb(double a) {
    MutProb = a;
  };

  // return Verbose
  int getVerbose() {
    return Verbose;
  };
  void setVerbose(int i) {
    Verbose = i;
  };

  //return PrintBestPop
  bool getPrintBestPop()const {
    return PrintBestPop;
  };
  void setPrintBestPop(bool tf) {
    PrintBestPop = tf;
  };

  //return result_dir
  string getResultDir()const {
    if (ResultDir.empty()) {
      ERR.General("Optimiser", __func__, " No Setting for the result directory");
    }
    return ResultDir;
  };
  void setResultDir(string dir) {
    ResultDir = dir;
  };

  //return ckpoint_dir
  string getCkpointDir()const {
    if (CkpointDir.empty()) {
      ERR.General("Optimiser", __func__, " No Setting for the result directory");
    }
    return CkpointDir;
  };
  void setCkpointDir(string dir) {
    CkpointDir = dir;
  };

  // for Mu : SA
  double getMu()const {
    if (Mu < 0) {
      ERR.General("Optimiser", __func__, "Invalid factor for Simulated annealing");
    }
    return Mu;
  };
  void setMu(double a) {
    Mu = a;
  };

  // Userdefined functions: used by PGA
  // CT: But... then these should be function pointers!
  // AZ: Yes, bit pgapack only takes void*
  void *MyMutation;             //   polynomialMutation
  void *MyCrossOver;            // linearCrossover,blxAlphaCrossover, simulatedBinaryCrossover
  void *MyEndOfGen;

protected:

  int PopSize;
  int NumBest;
  int NumGen;
  double MutProb;

  int MaxSimilarity;
  int MaxNoChange;

  int CkpointStartNum;

  int RestartFreq;

  //case PSO
  int PSONumGen;
  double Congnitive;
  double Inertia;
  double Social;

  //case Anneal
  double Mu;

  //for Rand
  int Seed;
  long long int SobolSeed;

  // verbose level
  int Verbose;
  int PrintFreq;

  string OutBestPopfile;

  string ResultDir;
  string CkpointDir;

  int CkpointFreq;

  double MINCOST;

  bool PrintBestPop;


  OptimiserType optimisertype;
  SetupType setuptype;
};


#endif
