/********************************************************************/
/*!
  @file Solver.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.3 $

*/
/******************************************************************/

#include <string>
#include <vector>
#include <exception>
using namespace std;

#ifndef __Solver_h__
#define __Solver_h__

#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>

#include <cModel.h>
#include <TimeSeries.h>
#include <CostArg.h>


class Solver
{
public:

  virtual ~Solver() {};


  static Solver& SetSolver(const SolverType s);

  virtual bool Solve() = 0;
  virtual bool SolveInput() = 0;
  virtual bool SolveInit() = 0;
  virtual bool SolveCreate() = 0;


  void SetModel(cModel *m) {
    model = m;
  };

  void SetTimeSeries(TimeSeries *ts) {
    t_series = ts;
  };

  int   CheckFlag(void *flagvalue, const char *funcname, int opt);


  cModel     *model;
  TimeSeries *t_series;

};

#endif
