#include <cmath>
using namespace std;

#ifndef __CVODESolver_h__
#define __CVODESolver_h__

#include <Solver.h>

#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>

class CVODESolver: public Solver
{

public:
  CVODESolver() {};

  // Implement the interface
  virtual bool Solve();
  virtual bool SolveCreate();
  virtual bool SolveInit();
  virtual bool SolveInput();

  //private:
protected:

  bool cvs_solver( );
  int   cvs_f( realtype t, N_Vector y, N_Vector ydot);
  // int   CheckFlag(void *flagvalue, const char *funcname, int opt);

  // Used to bounce back to C++ proper with f_data set to "this"
  static int   cvs_f_bouncer( realtype t, N_Vector y, N_Vector ydot, void *f_data);

  void *solver_mem;

  vector<double> y_in;

  vector<vector <double> > results;

  N_Vector y_cvs;
  N_Vector abstol;

};
class TimeVectorCVODESolver: public CVODESolver
{
public:
  //list of time points to calculate system at.
  // NB! T0 is not part of the vector.
  // First point in it should be first point after T0
  vector<double> time_points;
  void create_time_points(double t0, double interval, vector<double> *measure_points);
  virtual bool Solve();
protected:
  bool cvs_solver( );

};


#endif

