/********************************************************************/
/*!
  @file PGSWrapper.h
  @date 2008

  @author Anatoly Sorokin
  $Revision: 1.1.2.1 $

  $LastChangedBy: Azusa Yamaguchi$
  $LastChangedDate: 2010/03/29 $

*/
/******************************************************************/

#ifndef __PGSWrapper_h__
#define __PGSWrapper_h__

#include <Optimiser.h>


using namespace std;

#include <pgapack.h>

extern "C" {

  double evaluate (PGAContext *ctx, int p, int pop);

  void MyPrintString(PGAContext *ctx, FILE *file,  int p, int pop) ;

}


class PGSWrapper : public Optimiser
{
public:
  PGSWrapper();
  virtual ~PGSWrapper();

  /*
   * Initialize all parameters for genetic algorithm and
   * create PGA context
   */
  virtual void Initialise();

  /*
   * execute the Optimiser run
   */
  virtual int Run();

  /*
   * destroy PGA context and summerize
   */
  virtual void Destroy();

  /*
   *  execute of parallell genetic algorithm
   */
  int PGSWrapper_Run();

  //void PrintRes(string fname);

  void N_InitString(PGAContext *ctx, int p, int pop);

  /*
   * Set the current genes into the ParamSet
   */
  void SetParamSet(PGAContext *ctx);

  /*
   * functions at each end of generation
   */
  virtual void endOfGen(PGAContext *ctx);

  /*
   * Check the stopping rule
   */
  virtual int StopCond(PGAContext *ctx, bool out);

  /*
   * return the best gene
   */
  vector<double> GetTheBest() ;

  PGAContext* GetContext()const {
    return ctx;
  };

  /*
   * Set a genetic algorithm logfile
   */
  void SetLogfile();

  /*
   * Get a costvalue of a gene
   */
  double GetScore(PGAContext *ctx, int p, int pop) ;

  void SetVerbose(int v) {
    verbose = v;
  };

  void SetFixedSeed(int seed) {
    fixedSeed = seed;
  };

  void SetRestartFreq(int freq) {
    restartFreq = freq;
  };

  void SetPrintBestPop() {
    bPrintBestPop = optarg->getPrintBestPop();
  };

  bool IsPrintBestPop() {
    return bPrintBestPop;
  };

  void SetPopulationFile(string popFile) {
    populationFileName = popFile;
    bReadPop = true;
  };

  void summarise();


protected:

  int popPerplaceType;
  int setNoDuplicates;
  int fixedSeed;
  int restartFreq;
  int verbose;

  PGAContext *ctx;

  int rank;
  double trackArr[6];
  int numEval;
  double timeEval;
  string runName;

  double *Upper;
  double *Lower;


  double reflect(double initPoint, double step, double boundary); // boundary condition

  void printPop();
private:
  vector<double> bestSoFar;
  bool bPrintBestPop;
  bool bReadPop;

  string populationFileName;
};

#endif

