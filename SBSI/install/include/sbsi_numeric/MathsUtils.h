/*
 * Collection of simple mathematics utilities.
 *
 * Author: Ally Hume
 *
 */

#ifndef __MathsUtils_h__
#define __MathsUtils_h__

#include <vector>
using namespace std;

/*
 * Mathematics Utility class.
 */
class MathsUtils
{
public:

  /**
   *  Computes the mean of a vector of double values.
   *
   * @param data  data for which to compute the mean
   *
   * @return the mean of the data vales, or zero if the vector is empty.
   */
  static double mean(const vector<double>& data);
  
  /**
   * Computes the sum of the vector of double values.
   * 
   * @param data  data for which to compute the sum
   *
   * @return the sum of the data values.
   */
  static double sum(const vector<double>& data);
  
  /**
   * Computes the sum of the product of two vectors. The result is
   * data1[0]*data2[0] + data1[1]*data2[1] + ...
   *
   * The sum is only computed for those indexes for which there is
   * data in both vectors. Thus if one vector is larger than the other
   * the additional entries are ignored.
   * 
   * @param data1   vector of data values
   * @param data2   vector of data values
   *
   * @return the sum of the product of corresponding pairs of data values.
   */
  static double sumProduct(const vector<double>& data1, const vector<double>& data2);

  /**
   * Creates a vector of indexes starting from 0 increasing by 1 until
   * the specied size (exclusive).  For example, is size is 4 then the 
   * result vector will be [0, 1, 2, 3]
   *
   * @param size     size of the result vector
   * @param result   result vector. Any exising contents will be overwritten.
   */
  static void getIndexVector(int size, vector<double>& result);
  
  /**
   * Performs basic linear detreanding. Uses linear regression to fit a line
   * through the data then subtracts the line from the data. The method 
   * assumes a constant time interval in the given time series.
   *
   * @param y        data values of a time series
   * @param result   detrended time series
   */
  static void detrend(const vector<double>& y, vector<double>& result);
};

#endif
