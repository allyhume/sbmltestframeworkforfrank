/* include/config.h.  Generated from config.h.in by configure.  */
/* include/config.h.in.  Generated from configure.ac by autoheader.  */

/* Name of package */
#define PACKAGE "pgapack"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "BUG-REPORT-ADDRESS"

/* Define to the full name of this package. */
#define PACKAGE_NAME "PGAPACK"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "PGAPACK 1.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "pgapack"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.1"

/* Version number of package */
#define VERSION "1.1"
