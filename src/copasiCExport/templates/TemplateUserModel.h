using namespace std;

#ifndef __UserModel_h__
#define __UserModel_h__

#include <cModel.h>
#include <math.h>


// piecewise is a function defined in SBML
// TODO: piecewise actually takes a variable number of arguments

#define piecewise(x, b, y) ( (b ? x : y))
#define gt(x,y) ( (x > y))
#define lt(x,y) ( (x < y))
#define geq(x,y) ( (x >= y))
#define leq(x,y) ( (x <= y))
#define delay(x,y) ( x )

// TODO: add apropriate definitions for you. 
#define inf INFINITY
#define EXPONENTIALE 2.71828182845904523536
#define PI 3.14159265358979323
#define NaN 0

// TODO: implement these
#define sec(x) (x)
#define csc(x) (x)
#define cot(x) (x)
#define arcsec(x) (x)
#define arccsc(x) (x)
#define arccot(x) (x)
#define asech(x) (x)
#define acsch(x) (x) 
#define acoth(x) (x)

double factorial(int i);

#define FUNCTION_HEADERS
#include "copasiCExport.c"
#undef FUNCTION_HEADERS

class  USERMODEL:public cModel {
public: 

  USERMODEL();

  void inputModel(void) ;

  void getRHS(double * y_in, double t, double* yout); 
  void update_assignment_variables(vector<vector<double> > * results); 
  vector<double> calVarVec(int iv, vector<vector<double> > states); 
  void calCoef(); 
  void doAssignment(double * x, double T); 

#define SIZE_DEFINITIONS
#include "copasiCExport.c"
#undef SIZE_DEFINITIONS

  double x[N_ARRAY_SIZE_X];
  double y[N_ARRAY_SIZE_Y];   
  double p[N_ARRAY_SIZE_P];

  double x_c[N_ARRAY_SIZE_XC];
  double y_c[N_ARRAY_SIZE_YC]; 
  double p_c[N_ARRAY_SIZE_PC];
  
  double ct[N_ARRAY_SIZE_CT];
};
static cModel* getModel(){return new USERMODEL();}

#endif
