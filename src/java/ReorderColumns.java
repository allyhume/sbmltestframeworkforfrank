import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReorderColumns {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {

	if (args.length != 1) {
            System.err.println("Usage: reorderColumns <inFile>");
            return;
        }
	String inFilename = args[0];
	
	BufferedReader br = null;
	String line = "";
	 
	br = new BufferedReader(new FileReader(inFilename));
	
	Map<String, List<Double>> data = new HashMap<String, List<Double>>();
	List<String> columnNames = new ArrayList<String>();
	List<String> sColumnNames = new ArrayList<String>();
	List<String> cColumnNames = new ArrayList<String>();
	List<String> xColumnNames = new ArrayList<String>();
	List<String> tColumnNames = new ArrayList<String>();
	List<String> aColumnNames = new ArrayList<String>();
	
	// Read the header
	line = br.readLine();
	String[] cols = line.split(",");
	for (String col : cols) {
	    columnNames.add(col);
	    data.put(col, new ArrayList<Double>());
	    if (col.startsWith("X")) xColumnNames.add(col);
	    if (col.startsWith("T")) tColumnNames.add(col);
	    if (col.startsWith("S")) sColumnNames.add(col);
	    if (col.startsWith("C")) cColumnNames.add(col);
	    if (col.startsWith("A")) aColumnNames.add(col);
	}
	
	while ((line = br.readLine()) != null) {
	    // use comma as separator
	    cols = line.split(",");
	    for (int i=0; i<cols.length; ++i) {
		data.get(columnNames.get(i)).add(Double.parseDouble(cols[i]));
	    }
	}

	List<String> sortedColumnNames = new ArrayList<String>();
	sortedColumnNames.add("time");
	Collections.sort(xColumnNames);
	sortedColumnNames.addAll(xColumnNames);
	Collections.sort(tColumnNames);
	sortedColumnNames.addAll(tColumnNames);
	Collections.sort(sColumnNames);
	sortedColumnNames.addAll(sColumnNames);
	Collections.sort(cColumnNames);
	sortedColumnNames.addAll(cColumnNames);
	Collections.sort(aColumnNames);
	sortedColumnNames.addAll(aColumnNames);
	
	// Print out the header
	for (int i=0; i<sortedColumnNames.size(); ++i) {
	    if (i!=0) System.out.print(",");
	    System.out.print(sortedColumnNames.get(i));
	}
	System.out.println();
	
	// Print out the data
	for (int i=0; i<data.get(sortedColumnNames.get(0)).size(); ++i) {
	    for (int j=0; j<sortedColumnNames.size(); ++j) {
		if (j!=0) System.out.print(",");
		System.out.print(data.get(sortedColumnNames.get(j)).get(i));
	    }
	    System.out.println();
	}
	
	br.close();
    }
}
